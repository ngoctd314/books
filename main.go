package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

type Page struct {
	views uint32
}

func (page *Page) SetViews(n uint32) {
	atomic.StoreUint32(&page.views, n)
}

func (page *Page) Views() uint32 {
	return atomic.LoadUint32(&page.views)
}

func main() {
	p := &Page{
		views: 0,
	}

	wg := &sync.WaitGroup{}

	var i uint32
	for i = 0; i < 1000; i++ {
		wg.Add(1)
		go func(k uint32) {
			p.SetViews(k)
			wg.Done()
		}(i)
	}

	wg.Wait()

	fmt.Println(p.Views())
}
